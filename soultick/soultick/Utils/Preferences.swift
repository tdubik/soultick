//
//  Preferences.swift
//  soultick
//
//  Created by Tomasz Dubik on 24/12/2019.
//  Copyright © 2019 VESTIONE Sp. z o.o. All rights reserved.
//

import Foundation

enum Preferences : String {
    case FirstStart = "first.start"
}
