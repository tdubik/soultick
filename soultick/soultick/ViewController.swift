//
//  ViewController.swift
//  soultick
//
//  Created by Tomasz Dubik on 23/12/2019.
//  Copyright © 2019 VESTIONE Sp. z o.o. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var gradientView: GradientView?
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        self.navigationItem.title = "Soul Tick"

        gradientView = GradientView(frame:self.view.bounds, colors:[Colors.violett.value, Colors.gray.value] )
        self.view.addSubview(gradientView!)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        let timerViewController = TimerViewController()
        self.navigationController?.pushViewController(timerViewController, animated: false)

    }


}

