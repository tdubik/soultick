//
//  Timer.swift
//  soultick
//
//  Created by Tomasz Dubik on 24/12/2019.
//  Copyright © 2019 VESTIONE Sp. z o.o. All rights reserved.
//

import UIKit

class TimerView: UIView {

    var bgPath: UIBezierPath!
    var shapeLayer: CAShapeLayer!
    var progressLayer: CAShapeLayer!

    override init(frame: CGRect) {
        super.init(frame: frame)
        simpleShape()
    }

    required init?(coder: NSCoder) {
        super.init(coder:coder)
    }

    var progress: Float = 0 {
         willSet(newValue)
         {
             progressLayer.strokeEnd = CGFloat(newValue)
         }
    }

    private func createCirclePath() {
        let x = self.frame.width/2
        let y = self.frame.height/2
        let center = CGPoint(x: x, y: y)

        bgPath = UIBezierPath()
        bgPath.addArc(withCenter: center, radius: x/CGFloat(2), startAngle: CGFloat(CGFloat(-0.5 * Double.pi)), endAngle: CGFloat(1.5 * Double.pi), clockwise: true)
        bgPath.close()
    }

    override var bounds: CGRect {
        didSet {
            createCirclePath()
            shapeLayer.path = bgPath.cgPath
            progressLayer.path = bgPath.cgPath
        }
    }

    func simpleShape() {
        createCirclePath()
        shapeLayer = CAShapeLayer()
        shapeLayer.path = bgPath.cgPath
        shapeLayer.lineWidth = 15
        shapeLayer.fillColor = nil
        shapeLayer.strokeColor = UIColor.lightGray.cgColor

        progressLayer = CAShapeLayer()
        progressLayer.path = bgPath.cgPath
        progressLayer.lineWidth = 15
        progressLayer.lineCap = CAShapeLayerLineCap.round
        progressLayer.fillColor = nil
        progressLayer.strokeColor = UIColor.red.cgColor
        progressLayer.strokeEnd = 0.0

        self.layer.addSublayer(shapeLayer)
        self.layer.addSublayer(progressLayer)
    }

}
