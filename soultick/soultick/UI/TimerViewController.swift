//
//  TimerViewController.swift
//  soultick
//
//  Created by Tomasz Dubik on 24/12/2019.
//  Copyright © 2019 VESTIONE Sp. z o.o. All rights reserved.
//

import UIKit

class TimerViewController: UIViewController {
    var timerView:TimerView!
    var startButton:UIButton!
    var stopButton:UIButton!
    var gradientView:GradientView!
    var timer:Timer!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    override func loadView() {
        super.loadView()

        gradientView = GradientView(frame: self.view.frame, colors: [Colors.violett.value, Colors.gray.value])
        
        self.view.addSubview(gradientView)

        timerView = TimerView()
        self.view.addSubview(timerView)
        
        startButton = UIButton()
        startButton.addTarget(self, action: #selector(startDidPress), for: .touchUpInside)
        startButton.setTitle(NSLocalizedString("Start", comment: "start button"), for: .normal)
        startButton.setTitleColor(.red, for: .normal)
        self.view.addSubview(startButton)

        stopButton = UIButton()
        stopButton.addTarget(self, action: #selector(stopDidPress), for: .touchUpInside)
        stopButton.isEnabled = false
        stopButton.setTitle(NSLocalizedString("Stop", comment: "start button"), for: .normal)
        stopButton.setTitleColor(.red, for: .normal)
        self.view.addSubview(stopButton)
        self.setupInitialConstraints()
    }

    func setupInitialConstraints(){
        self.timerView.translatesAutoresizingMaskIntoConstraints = false
        self.stopButton.translatesAutoresizingMaskIntoConstraints = false
        self.startButton.translatesAutoresizingMaskIntoConstraints = false

        let views = ["startButton" : startButton, "stopButton" : stopButton, "view" : self.view, "timerView" : timerView]
        let metrics = ["buttonOffset" : 100, "timerOffset" : 50]

        self.view.addConstraint(NSLayoutConstraint(item: timerView!, attribute: .centerX, relatedBy: .equal, toItem: self.view, attribute: .centerX, multiplier: 1.0, constant: 0))

        self.view.addConstraint(NSLayoutConstraint(item: timerView!, attribute: .centerY, relatedBy: .equal, toItem: self.view, attribute: .centerY, multiplier: 1.0, constant: -100))

        self.view.addConstraint(NSLayoutConstraint(item: timerView!, attribute: .width, relatedBy: .equal, toItem: self.view, attribute: .width, multiplier: 1.0, constant: -100))

        self.view.addConstraint(NSLayoutConstraint(item: timerView!, attribute: .height, relatedBy: .equal, toItem: timerView, attribute: .width, multiplier: 1.0, constant: -100))

        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(==buttonOffset)-[startButton]", options: NSLayoutConstraint.FormatOptions(), metrics: metrics, views: views as [String : Any]))

        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:[stopButton]-(==buttonOffset)-|", options: NSLayoutConstraint.FormatOptions(), metrics: metrics, views: views as [String : Any]))

        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[startButton(==30)]-(==200)-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: views as [String : Any]))

        self.view.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:[stopButton(==30)]-(==200)-|", options: NSLayoutConstraint.FormatOptions(), metrics: nil, views: views as [String : Any]))

    }

    @objc func startDidPress() {
        start()
    }

    private func start() {
        var counter = 0
        timerView.progress = 0

        timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true, block: { (timer) in
            counter += 1
            self.timerView.progress = Float(Float(counter)/Float(100))
            if counter == 100 {
                timer.invalidate()
                print("timer stopped")
                self.stop()
            }
        })
        startButton.isEnabled = false
        stopButton.isEnabled = true
    }

    @objc func stopDidPress() {
        stop()
    }

    private func stop() {
        timer.invalidate()
        startButton.isEnabled = true
        stopButton.isEnabled = false
    }

}
