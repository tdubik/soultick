//
//  GradientView.swift
//  soultick
//
//  Created by Tomasz Dubik on 24/12/2019.
//  Copyright © 2019 VESTIONE Sp. z o.o. All rights reserved.
//

import UIKit

class GradientView: UIView {
    let gradientLayer = CAGradientLayer()

    var colors:[UIColor] = [] {
        willSet{
            gradientLayer.colors = self.colors
        }
    }

    override var bounds: CGRect {
        didSet {
            gradientLayer.frame = self.frame
        }
    }

    init(frame: CGRect, colors:[UIColor] = []) {
        super.init(frame: frame)
        setupLayer(colors)
    }

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        setupLayer()
    }

    private func setupLayer(_ colors:[UIColor] = []) {
        self.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        gradientLayer.frame = self.frame
        self.layer.addSublayer(gradientLayer)

        gradientLayer.colors = [UIColor.red, UIColor.white]
    }

}
